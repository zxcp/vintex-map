<?php /** Created by Anton on 24.09.2018. */ ?>

<!DOCTYPE html>
<html lang="ru-RU" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">    <link rel='stylesheet'  href='https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.min.css' type='text/css'>
    <link rel="stylesheet" href="../css/style.css" type="text/css">
</head>
<body>
	<div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 col-sm-offset-3 col-md-offset-4">
                <form action="add.php" method="post">
                    <h3 class="form-signin-heading text-center">Добавить материал</h3>
                    <input class="form-control form-group" name="name" placeholder="Материал">
                    <div class="form-group">
                        <select class="selectpicker form-control form-group" name="tags[]" multiple data-selected-text-format="count>4">
                            <option>tag1</option>
                            <option>tag2</option>
                            <option>tag3</option>
                            <option>tag4</option>
                        </select>
                    </div>
                    <textarea class="form-control form-group" name="desc" placeholder="Описание"></textarea>
                    <button class="btn btn-primary btn-block" type="submit">Сохранить</button>
                </form>
            </div>
        </div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/js/bootstrap-select.min.js"></script>
</body>
</html>