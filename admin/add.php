<?php /** Created by Anton on 24.09.2018. */

$name = $_POST['name'];
$tags = $_POST['tags'];
$desc = $_POST['desc'];

if ($name && $tags) {
    require_once ('../classes/DB.php');
    DB::insert($name, $tags, $desc);
    header('Location: http://localhost/admin/');
}