<?php /** Created by Anton on 24.09.2018. */

class DB {
    private static $_host = '127.0.0.1';
    private static $_db   = 'vintex';
    private static $_user = 'root';
    private static $_pass = 'root';
    private static $_charset = 'utf8';
    private static $_opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    private function _getPDO() {
        $dsn = 'mysql:host=' . self::$_host . ';dbname=' . self::$_db . ';charset=' . self::$_charset . ';';
        return new PDO($dsn, self::$_user, self::$_pass, self::$_opt);
    }

    static function insert($name, $tags, $desc) {
        $stmt = self::_getPDO()->prepare('INSERT INTO materials (`name`, `tags`, `desc`) VALUES (? , ?, ?)');
        $stmt->execute([$name, serialize($tags), $desc]);
        return $stmt->fetchAll();
    }

    static function select($tag = null) {
        if ($tag) $stmt = self::_getPDO()->prepare('SELECT * FROM materials WHERE `tags` LIKE ? LIMIT 50');
        else $stmt = self::_getPDO()->prepare('SELECT * FROM materials LIMIT 50');
        $stmt->execute($tag ? ['%' . $tag . '%'] : []);
        return $stmt->fetchAll();
    }

}