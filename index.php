<?php
    require_once ('classes/DB.php');
?>

<!DOCTYPE html>
<html lang="ru-RU" prefix="og: http://ogp.me/ns#"">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Vintex test 1</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">    <link rel='stylesheet'  href='https://cdn.bootcss.com/bootstrap-select/2.0.0-beta1/css/bootstrap-select.min.css' type='text/css'>
    <link rel='stylesheet'  href='css/style.css' type='text/css'>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 col-sm-offset-3 col-md-offset-4">
                <h2 class="text-center">Материалы</h2>
                <?php foreach (DB::select($_GET['tag']) as $material): ?>
                    <div class="main-block_con text-center">
                        <div class="main-block_con--title"><?= $material['name'] ?></div>
                        <div class="main-block_con--tags">
                        <?php foreach (unserialize($material['tags']) as $tag): ?>
                            <a class="btn btn-primary btn-sm" href="/?tag=<?= $tag ?>" target="_blank"><?= $tag ?></a>
                        <?php endforeach; ?>
                        </div>
                        <div class="main-block_con--desc"><?= $material['desc'] ?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>